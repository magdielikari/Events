var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var cleanCSS  = require('gulp-clean-css');

const paths = {
  styles: {
    src: 'sass/*.scss',
    dest: 'style/'
  }
};

function sassTask(done){
	gulp.src(paths.styles.src)
		.pipe(sass())
//		.pipe(cleanCSS())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(paths.styles.dest))
	done();
}
sassTask.description = 'Compile the sass files';

gulp.task('sass', sassTask);
var all = gulp.series('sass');

gulp.task('default', all);
