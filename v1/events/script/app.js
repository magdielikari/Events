var app = angular.module("app", [
	"ui.router",
	"ngDialog",
	"moment-picker",
	"ngCookies",
	"ngMeta"
]);

app.config([
	"$stateProvider",
	"$urlRouterProvider",
	"$locationProvider",
	"ngMetaProvider",
	function( $stateProvider, $urlRouterProvider, $locationProvider, ngMetaProvider ) {
		$locationProvider.html5Mode(true);
		$urlRouterProvider.otherwise("/");
		var imagen = document.location.origin + '/img/event.png';
		
		$stateProvider.state("master", {
			url: "/",
			controller: "MasterController",
			templateUrl: "views/list.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'website',
					'og:locale': 'en_US',
					'title': 'List of Fuckton Events',
					'og:title': 'List of Fuckton Events',
					'description': 'You can review, filter, update and delete Fuckton Events',
					'og:image': imagen
				}
			},
			resolve: {
				explorer: function(ProviderService){
					return ProviderService.explorer();
				}
			}
		});

		$stateProvider.state("detail", {
			url: "/event/:id",
			controller: "DetailController",
			templateUrl: "views/detail.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Details of Fuckton Event',
					'og:title': 'Detail of Fuckton Event',
					'description': 'You can share the most important information of an Fuckton Event',
					'og:image': imagen
				}
			},
			resolve: {
				activity: function(ProviderService, $stateParams){
					return ProviderService.activity($stateParams.id);
				}
			}
		});
//1
		$stateProvider.state("map", {
			url: "/map",
			controller: "MapController",
			templateUrl: "views/map.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Details of Fuckton Event',
					'og:title': 'Detail of Fuckton Event',
					'description': 'You can share the most important information of an Fuckton Event',
					'og:image': imagen
				}
			},
			resolve: {
				activity: function(ProviderService){
					return ProviderService.activities();
				}
			}
		});
//2
		$stateProvider.state("calendar", {
			url: "/calendar/:i",
			controller: "CalendarController",
			templateUrl: "views/calendar.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Details of Fuckton Event',
					'og:title': 'Detail of Fuckton Event',
					'description': 'You can share the most important information of an Fuckton Event',
					'og:image': imagen
				}
			},
			resolve: {
				plans: function(ProviderService, $stateParams){
					return ProviderService.plans($stateParams.i);
				}
			}
		});

//3
		$stateProvider.state("route", {
			url: "/route/:ii",
			controller: "RouteController",
			templateUrl: "views/route.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Details of Fuckton Event',
					'og:title': 'Detail of Fuckton Event',
					'description': 'You can share the most important information of an Fuckton Event',
					'og:image': imagen
				}
			},
			resolve: {
				plans: function(ProviderService, $stateParams){
					return ProviderService.plans($stateParams.ii);
				}
			}
		});

//4
		$stateProvider.state("plans", {
			url: "/plans/:iii",
			controller: "PlansController",
			templateUrl: "views/plans.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Details of Fuckton Event',
					'og:title': 'Detail of Fuckton Event',
					'description': 'You can share the most important information of an Fuckton Event',
					'og:image': imagen
				}
			},
			resolve: {
				plans: function(ProviderService, $stateParams){
					return ProviderService.plans($stateParams.iii);
				}
			}
		});
//5
		$stateProvider.state("plan", {
			url: "/plan/",
			controller: "PlanController",
			templateUrl: "views/details.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Create a Fuckton Event',
					'og:title': 'Create a Fuckton Events',
					'description': 'Record new Fuckton Events so that you can then share them all',
					'og:image': imagen
				}
			},
			resolve: {
				newPlan: function(){
					var data = {
						"user" : 1,
						"events": [1]
					}
					return data;   
				}
			}		
		});

		$stateProvider.state("update", {
			url: "/update/:iid",
			controller: "UpdateController",
			templateUrl: "views/create.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Update a Fuckton Event',
					'og:title': 'Update a Fuckton Event',
					'description': 'Something changed in your Fuckton event, change it',
					'og:image': imagen
				}
			},
			resolve: {
				activity: function(ProviderService, $stateParams){
					return ProviderService.activity($stateParams.iid);
				}
			}
		});

		$stateProvider.state("create", {
			url: "/create/",
			controller: "CreateController",
			templateUrl: "views/create.html",
			data: {
				'meta': {
					'author': 'Potencial Estudio',
					'og:type': 'Website',
					'og:locale': 'en_US',
					'title': 'Create a Fuckton Event',
					'og:title': 'Create a Fuckton Events',
					'description': 'Record new Fuckton Events so that you can then share them all',
					'og:image': imagen
				}
			},
			resolve: {
				newActivity: function(){
					var data = {
						"first_name": "Blockchain Bootcamo",
						"last_name": "Blockchain Trainer",
						"price": "100$",
						"src": "/img/qr.png",
						"capacity": "103 people Max",
						"committed": "5 people going",
						"start": {
						  "day": 29,
						  "month": "Jan",
						  "year": 2018,
						  "hour": "01:05 pm",
						  "name": "Wednesday",
						  "date": "01-29-2018"
						},
						"end": {
						  "day": 22,
						  "month": "Feb",
						  "year": 2018,
						  "hour": "06:30 pm",
						  "name": "friday",
						  "date": "02-22-2018"
						},
						"iCal": true,
						"notification": true,
						"place": "Shekoru 17",
						"region": "Shenzhen",
						"on": "Eventrite",
						"description" : "Instuctor Led Online",
						"type": "Video Conference",
						"map": "Coordenadas",
						"contact_phone": 5555555
					}
					return data;   
				}
			}		
		});
	}
]);

app.config(['ngDialogProvider', function (ngDialogProvider) {
	ngDialogProvider.setDefaults({
		className: 'ngdialog-theme-default',
		plain: false,
		showClose: true,
		closeByDocument: true,
		closeByEscape: true,
		appendTo: false,
		preCloseCallback: function () {
			console.log('default pre-close callback');
		}
	});
}]);

app.config(['momentPickerProvider', function (momentPickerProvider) {
	momentPickerProvider.options({
		/* Picker properties */
		locale:        'en',
		format:        'LTS',
		minView:       'decade',
		maxView:       'minute',
		startView:     'year',
		autoclose:     true,
		today:         false,
		keyboard:      false,
		
		/* Extra: Views properties */
		leftArrow:     '&larr;',
		rightArrow:    '&rarr;',
		yearsFormat:   'YYYY',
		monthsFormat:  'MMM',
		daysFormat:    'D',
		hoursFormat:   'h a',
		minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
		secondsFormat: 'ss',
		minutesStep:   5,
		secondsStep:   1
	});
}]);

app.run(['ngMeta','$rootScope', '$state', '$window', function(ngMeta, $rootScope, $state, $window) { 
	ngMeta.init();
}]);

app.constant('userActive', {
	id: 1,
	name: "Lucas Roitman",
	eventsId: [0, 1]
})